/* Övning 501: score

Skapa en struct med namnet Score. Den ska ha
attributen name (String) och score (f64).

Se till att Score har lämpliga traits, bl.a.
PartialOrd (jämför score oberoende av name).

I main-funktionen, skapa en Vector av Score.
Sortera vektorn och skriv ut den.

*/




