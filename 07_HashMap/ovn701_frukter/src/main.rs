/* Övning 701: frukter

Skapa funktionen kilopris. Den ska ta en referens
till en HashMap::<&str, f64> som parameter.
Nycklarna är namn på frukter, och värdena är kilopris.
Funktionen ska skriva ut frukterna, en per rad, sorterade
i växande ordning efter kilopriset. T.ex.
Banan kostar 25.00 kr
Päron kostar 29.75 kr

 */



