/* Övning 301: Gissa tal

Välj ett slumptal mellan 1 och 1000. Låt användaren
gissa vilket talet är. Om gissningen är för liten
eller för stor, så ska programmet säga det och sedan
låta användaren gissa igen. Ifall gissningen är rätt
ska programmet berätta det och sedan avslutas. T.ex.

Gissa tal (1-1000): 500
För litet. Gissa tal (1-1000): kalle
Ej tal! Gissa tal (1-1000): 600
För stort. Gissa tal (1-1000): 510
För stort. Gissa tal (1-1000): 505
Rätt efter 4 gissningar!

*/

use std::io::{stdin, stdout, Write};

fn main() {

}
