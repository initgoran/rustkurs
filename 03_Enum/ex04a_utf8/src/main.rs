/*  ex04a_utf8  */
fn main() {
    let namn: &str = "Göran";
    println!("Namn: {} (längd {})", namn, namn.len());
}
// Namn: Göran (längd 6)
