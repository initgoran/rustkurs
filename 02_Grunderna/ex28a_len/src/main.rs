/*  ex28a_len  */
fn main() {
    let p = String::from("Göran");
    println!("Storlek: {}", p.len());
    println!("Längd: {}", p.chars().count());
}
// Storlek: 6
// Längd: 5
