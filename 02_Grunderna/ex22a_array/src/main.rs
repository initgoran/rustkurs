/*  ex22a_array  */
fn main() {
    let a: [usize; 5] = [12, 19, 33, 27, 22];
    for i in 0..5 {
        print!("{} ", a[i]);
    }
}
// 12 19 33 27 22
