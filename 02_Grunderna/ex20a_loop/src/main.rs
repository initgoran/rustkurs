/*  ex20a_loop  */
fn main() {
    for i in 1..5 {
        println!("{}", i);
    }
}
// 1
// 2
// 3
// 4
