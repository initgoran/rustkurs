/*  ex14a_kvadratrot  */
fn main() {
    let x: f64 = 1.1;
    println!("Kvadrat: {:.3}", x * x);
    let y: f32 = 2.0;
    println!("Rot: {:.6}", y.sqrt());
}
// Kvadrat: 1.210
// Rot: 1.414214
