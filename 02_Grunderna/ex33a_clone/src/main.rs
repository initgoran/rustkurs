/*  ex33a_clone  */
fn main() {
    let namn = String::from("Steve");
    let p = namn.clone();
    println!("Namn: {} och {}", namn, p);
}
// Namn: Steve och Steve
