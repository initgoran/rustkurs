/*  ex09a_ok  */
fn max<'a>(v1: &'a [String], v2: &'a [String]) -> &'a String {
    let s1 = v1.iter().max_by_key(|s| s.len()).unwrap();
    let s2 = v2.iter().max_by_key(|s| s.len()).unwrap();
    if s1.len() > s2.len() { s1 }  else { s2 }
}

fn main() {
    let n1: Vec<_> = vec!["Bill".to_string(), "Steve".to_string()];
    let n2: Vec<_> = vec!["Linus".to_string(), "Ken".to_string()];
    let m = max(&n1, &n2);
    println!("Värdet är {}", *m);
}
// Värdet är Linus
