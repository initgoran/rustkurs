/*  ex09e_missing_tick  */
fn max(v1: &[String], v2: &[String]) -> &String {
    let s1 = v1.iter().max_by_key(|s| s.len()).unwrap();
    let s2 = v2.iter().max_by_key(|s| s.len()).unwrap();
    if s1.len() > s2.len() { s1 }  else { s2 }
}
// error[E0106]: missing lifetime specifier

